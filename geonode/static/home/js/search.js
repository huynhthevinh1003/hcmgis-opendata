$('.header-search').on('click', '.search-toggle-search', function(e) {
    var selector = $(this).data('selector');
  
    $(selector).toggleClass('show').find('.search-input-search').focus();
    $(this).toggleClass('active');
  
    e.preventDefault();
});