# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, render
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse

from django.db.models import F

from geonode.msource import const
from geonode.msource.forms import MSourceForm
from .forms import *
from geonode.msource.models import MSource
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

def list_view_msource(request):
    list_msource = MSource.objects.filter(status=const.MODEL_STATUS_VISIBLE).order_by('-id').all()

    return render(request, 'msource/list_view_msource.html', {'list_msource': list_msource})

def index_msource(request):
    if request.user.is_authenticated():
        list_msource = MSource.objects.order_by('-id').all()

        return render(request, 'msource/index.html', {'list_msource': list_msource})
    return HttpResponseRedirect(reverse('msource.views.list_view_msource'))

def create_msource(request):
    form = MSourceForm()
    if request.method == 'POST':
        form = MSourceForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, u'<b>%s</b> created successfully'%form.cleaned_data['title'])
            return HttpResponseRedirect(reverse('msource.views.list_view_msource'))
    return render(request, 'msource/create.html', {'form': form})

def update_msource(request):
    id = request.GET.get('id', -1)
    if request.user.is_authenticated():
        model = MSource.objects.filter(id=id).first()

        if model is None:
            return HttpResponseRedirect(reverse('msource.views.create_msource'))

        form = MSourceForm(instance=model)
        if request.POST:
            form = MSourceForm(request.POST, instance=model)
            if form.is_valid():
                model = form.save()
                model.save()
                messages.success(request, u'<b>%s</b> updated successfully'%form.cleaned_data['title'])
                return HttpResponseRedirect(reverse('msource.views.index_msource'))
            else:
                return HttpResponse("Cập nhật không thành công")
    
        return render(request, 'msource/update.html', {'form': form, 'model': model})
    return HttpResponseRedirect(reverse('msource.views.index_msource'))

def delete_msource(request):
    id = request.GET.get('id', -1)
    if request.user.is_authenticated():
        if id:
            model = MSource.objects.filter(id=id).first()
            if model:
                model.status = const.MODEL_STATUS_DELETE
                model.save()
                messages.success(request, u"Xóa <b>%s</b> thành công" % model.title)
                return HttpResponseRedirect(reverse('msource.views.index_msource'))

    return HttpResponseRedirect(reverse('msource.views.index_msource'))

def msource_filter_user(request):
    user_filter = request.user.id

    if user_filter:
        list_msource_filter_user = MSource.objects.filter(user=user_filter).order_by('-id').all()
    
    return render(request, 'msource/list_view_msource_filter_user.html', {'list_msource_filter_user': list_msource_filter_user})

def detail_msource(request, slug):
    if slug:
        MSource.objects.filter(slug=slug).update(views=F('views') + 1)
        list_detail = MSource.objects.filter(slug=slug).all()
    return render(request, 'msource/detail.html',{'list_detail': list_detail})

def accept_msource(request):
    id = request.GET.get('id', -1)
    if request.user.is_authenticated():
        if id:
            model = MSource.objects.filter(id=id).first()
            if model:
                model.status = const.MODEL_STATUS_VISIBLE
                model.save()
                return HttpResponseRedirect(reverse('msource.views.index_msource'))

    return HttpResponseRedirect(reverse('msource.views.index_msource'))

def deny_msource(request):
    id = request.GET.get('id', -1)
    if request.user.is_authenticated():
        if id:
            model = MSource.objects.filter(id=id).first()
            if model:
                model.status = const.MODEL_STATUS_INVISIBLE
                model.save()
                return HttpResponseRedirect(reverse('msource.views.index_msource'))

    return HttpResponseRedirect(reverse('msource.views.index_msource'))