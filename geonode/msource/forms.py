from django import forms
from django.forms import ModelForm
from geonode.msource import models as msource_models  
from geonode.msource.models import MSource  

class MSourceForm(forms.ModelForm):

    class Meta:
        model = msource_models.MSource
        exclude = []
