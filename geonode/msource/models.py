# -*- coding: utf-8 -*-
from datetime import date
from django.utils.text import slugify

from django.db import models
from django.conf import settings
from geonode.msource import const


# Create MSource
class MSource(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=500)
    descriptions = models.TextField()
    content = models.TextField()
    tag = models.CharField(max_length=100)
    date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    status = models.IntegerField(default=const.MODEL_STATUS_INVISIBLE, null=True, blank=True)
    views = models.IntegerField(default=0, blank=True, null=True)
    slug = models.SlugField(max_length=200, unique=True, blank=True, null=True)

    def _get_unique_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        num = 1
        while MSource.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_unique_slug()
        super(MSource, self).save(*args, **kwargs)

    def __str__(self):              
        return self.title
    