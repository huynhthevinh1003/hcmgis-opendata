# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from . import views as msource_views

urlpatterns = [
    url(r'^create/$', msource_views.create_msource, name='msource.views.create_msource'),
    url(r'^list-view/$', msource_views.list_view_msource, name='msource.views.list_view_msource'),
    url(r'^update/$', msource_views.update_msource, name='msource.views.update_msource'),
    url(r'^index-msource/$', msource_views.index_msource, name='msource.views.index_msource'),
    url(r'^my-source/$', msource_views.msource_filter_user, name='msource.views.msource_filter_user'),
    url(r'^delete/$', msource_views.delete_msource, name='msource.views.delete_msource'),
    url(r'^detail(?:/(?P<slug>[\w\d-]+))?/$', msource_views.detail_msource, name='msource.views.detail_msource'),
    url(r'^accept-msource/$', msource_views.accept_msource, name='msource.views.accept_msource'),
    url(r'^deny-msource/$', msource_views.deny_msource, name='msource.views.deny_msource'),
]